"""Tests for `cdaas` script."""

from os import chmod
from random import randint
from tempfile import TemporaryDirectory
from testtools import TestCase

import cdaas
import os.path
import stat
import yaml


def make_name(base_name='id'):
    """Return a random name string."""
    return '%s-%d' % (base_name, randint(0, 999999))


def make_file(location, content):
    """Write `content` to a new file in `location`."""
    path = os.path.join(location, 'config.yaml')
    with open(path, 'w') as stream:
        stream.write(content)
    return path


class TestReadConfig(TestCase):
    """Tests for `read_config`."""

    def make_location(self):
        """Create a temporary working directory.  Destroy after test."""
        location = TemporaryDirectory()
        self.addCleanup(location.cleanup)
        return location.name

    def make_config(self, location, content_object):
        """Write YAML config file in `location` containing given object."""
        return make_file(location, yaml.dump(content_object))

    def test_returns_None_if_not_found(self):
        self.assertIsNone(cdaas.read_config(make_name('nonfile')))

    def test_returns_None_if_dir_not_found(self):
        nonfile = os.path.join(make_name('nondir'), make_name('nonfile'))
        self.assertIsNone(cdaas.read_config(nonfile))

    def test_fails_on_other_access_error(self):
        myfile = self.make_config(self.make_location(), [])
        # Make the file unreadable (but keep "write" permission so we can
        # still delete it during cleanup).
        chmod(myfile, stat.S_IWUSR)
        self.assertRaises(Exception, cdaas.read_config, myfile)

    def test_returns_config(self):
        content = [randint(1, 99) for _ in range(5)]
        config = self.make_config(self.make_location(), content)
        self.assertEqual(content, cdaas.read_config(config))

    def test_fails_on_bad_syntax(self):
        config = make_file(self.make_location(), '}sdofj]')
        self.assertRaises(Exception, cdaas.read_config, config)


class TestDecodeIfBytes(TestCase):
    """Tests for `decode_if_bytes`."""

    def test_returns_str_as_is(self):
        content = make_name('random-string')
        self.assertEqual(content, cdaas.decode_if_bytes(content))

    def test_decodes_bytes(self):
        content = make_name('random-bytes')
        self.assertEqual(
            content, cdaas.decode_if_bytes(content.encode('utf-8')))

    def test_uses_given_encoding(self):
        content = make_name('random-bytes')
        encoding = 'utf-16-le'
        self.assertEqual(
            content,
            cdaas.decode_if_bytes(content.encode(encoding), encoding))
