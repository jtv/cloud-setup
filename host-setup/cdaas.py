#!/usr/bin/env python3

# Python 2 directive: "make 'print' a function just like in python 3."  Not
# needed since we're in python 3, but it makes some lint checkers understand
# that we can pass keyword arguments to print().
from __future__ import print_function

from argparse import ArgumentParser
from configparser import SafeConfigParser
from datetime import datetime
from fnmatch import fnmatch
from operator import itemgetter
from subprocess import check_call
from sys import version_info
from textwrap import dedent
from time import sleep
from urllib.request import (
    Request,
    urlopen,
    )
import yaml

import json
import os.path


# Program description.  Used as header for --help output.
# TODO: Complete (brief) description.
DESCRIPTION = dedent("""\
    Create server.
    """)


if version_info.major == 2:
    # Silence linters which don't get that we're in python 3.
    # No, this would not work in python 2 without a bit more work.
    FileNotFoundError = None


def read_config(path):
    """Read config file, return as `dict`, or `None` if not found."""
    try:
        with open(path, encoding='utf-8') as config:
            return yaml.load(config)
    except FileNotFoundError:
        return None


def parse_args():
    """Parse command-line arguments.  Return as namespace."""
    parser = ArgumentParser()
    subparsers = parser.add_subparsers()

    parser.add_argument(
        '-c', '--config', metavar='PATH', default='server.cfg',
        help="Config file.")
    parser.add_argument('-t', '--api-token')

    parser_create = subparsers.add_parser('create', help="Start new VM")
    parser_create.add_argument('server_name', help="Name for server.")
    parser_create.add_argument('-i', '--image')
    parser_create.add_argument('-o', '--organization')
    parser_create.add_argument(
        '-P', '--public-ip', action='store_true', default=False,
        help="Assign a public IP address.")
    parser_create.add_argument(
        '-a', '--ansible-dir', help="Working directory for Ansible.")
    parser_create.add_argument(
        '-p', '--ansible-playbook', help="Ansible playbook to run.")
    parser_create.add_argument(
        '-C', '--clash', metavar='RESPONSE',
        choices=('fail', 'ignore', 'rename', 'f', 'i', 'r'),
        default='rename',
        help=(
            "How to handle name collisions: fail ('f'), ignore ('i'), or "
            "rename ('r').  "
            "Default is 'rename': add unique suffix to the name.  "
            "The 'ignore' mode will blithely create a VM with a duplicate "
            "name."
            ))
    parser_create.set_defaults(func=create_server)

    parser_delete = subparsers.add_parser('delete', help="Destroy VM(s)")
    parser_delete.add_argument(
        'server_pattern', help="Glob pattern for servers' names or IDs.")
    parser_delete.set_defaults(func=delete_server)

    parser_list = subparsers.add_parser('list', help="List VMs")
    parser_list.add_argument(
        '-p', '--server-pattern', default='*', help="Glob for names or IDs.")
    parser_list.add_argument(
        '-l', '--long-format', action='store_true', default=False)
    parser_list.set_defaults(func=list_servers)

    if os.path.isfile('server.cfg'):
        conf_file = SafeConfigParser()
        conf_file.read("server.cfg")
        parser.set_defaults(**dict(conf_file.items("DEFAULT")))
        for sub_parser, sub_name in (
            (parser_create, 'create'),
            (parser_delete, 'delete'),
            (parser_list, 'list'),
            ):
            sub_parser.set_defaults(**dict(conf_file.items(sub_name)))

    return parser.parse_args()


def decode_if_bytes(content, encoding='utf-8'):
    """Return `content` as unicode `str`, decoding it if necessary.

    Technically, the correct encoding can be hard to figure out.  For now
    (and perhaps forever!) we only work with UTF-8.
    """
    if isinstance(content, bytes):
        # Looks like Python 3.5 and older return raw bytes.
        return content.decode(encoding)
    else:
        # Looks like Python 3.6 and newer return str.
        return content


class ScalewaySession:
    """Wrapper for the Scaleway REST API.

    :ivar service_url: Base URL for Scaleway's REST service.  Must end in a
        slash.
    :ivar api_token: Authentication token for use with the REST service.
    """

    def __init__(self, service_url, api_token, organization=None):
        self.service_url = service_url
        self.api_token = api_token
        self.organization = organization

    def _request(self, resource, method='GET', data=None):
        """Perform request, return object parsed from JSON response.

        :param resource: Path to resource, relative to the `service_url`.  The
            separating slash should be at the end of the `service_url`; the
            `resource` path does not start with a slash.
        :param method: HTTP method (`GET`, `POST`).
        :param data: Optional request body data.  If given, will be sent as
            JSON.
        """
        headers = {
            'X-Auth-Token': self.api_token,
            'Content-Type': 'application/json',
            }
        url = self.service_url + resource
        if data is not None:
            data = json.dumps(data, indent=4, sort_keys=True).encode('utf-8')
        req = Request(url=url, data=data, method=method, headers=headers)
        with urlopen(req) as response:
            result = response.read()
        return json.loads(decode_if_bytes(result))

    def _make_url_part(self, arg):
        """Represent `arg` for use in a resource path."""
        if isinstance(arg, dict):
            # This is an object.  Use its id.
            arg = arg['id']
        return str(arg)

    def _make_path(self, *args):
        """Compose a resource path from components."""
        return '/'.join(self._make_url_part(arg) for arg in args)

    def list_servers(self, glob='*'):
        """Return list of servers whose names or IDs match `glob`."""
        all_servers = self._request('servers')['servers']
        return [
            server
            for server in all_servers
            if matches_glob(server, glob)
            ]

    def list_ips(self):
        """Return list of allocated IP addresses."""
        return self._request('ips')['ips']

    def create_ip(self):
        """Allocate an IP address, and return an IP entry.

        Requires `organization` to be set.

        See Scaleway API definition for the structure of the return value.
        It's a `dict` containing, among other things, `id` and `address`.
        """
        assert self.organization is not None
        body = {'organization': self.organization}
        return self._request('ips', method='POST', data=body)['ip']

    def delete_ip(self, ip):
        """Deallocate IP address."""
        self._request(self._make_path('ips', ip))

    def create_server(self, name, image, ip, server_type='C2M', tags=[],
                      ipv6=False):
        """Start a server and return a Server entry."""
        assert self.organization is not None
        body = {
            'organization': self.organization,
            'name': name,
            'image': image,
            'commercial_type': server_type,
            'tags': tags,
            'dynamic_ip_required': True,
            'public_ip': ip['id'],
            'enable_ipv6': ipv6,
            }
        return self._request('servers', 'POST', body)['server']

    def get_server(self, server):
        """Retrieve server object."""
        return self._request(self._make_path('servers', server))['server']

    def delete_server(self, server):
        """Shut down & destroy server."""
        # Can't DELETE server when it's running.
        # TODO: Quietly accept 404 response.
        self.perform_action(server, 'terminate')

    def list_actions(self, server):
        """List available actions for `server`."""
        path = self._make_path('servers', server, 'action')
        return self._request(path)['actions']

    def perform_action(self, server, action):
        """Perform `action` on `server`."""
        path = self._make_path('servers', server, 'action')
        data = {'action': action}
        self._request(path, method='POST', data=data)


SERVICE_URL = 'https://cp-ams1.scaleway.com/'


def debug_output(header, obj):
    """For debugging: print `obj` as JSON."""
    print("\n*** %s ***" % header)
    print(json.dumps(obj, indent=4, sort_keys=True))
    print()


def list_servers(args):
    session = ScalewaySession(
        service_url=SERVICE_URL, api_token=args.api_token)
    servers = session.list_servers(args.server_pattern)
    servers = sorted(servers, key=itemgetter('name'))
    for server in servers:
        if args.long_format:
            print(json.dumps(server, indent=4, sort_keys=True))
        else:
            print(server['id'], server['name'], sep='\t')


def make_server(session, name, image):
    """Start a server, with its own IP."""
    ip = None
    server = None
    try:
        ip = session.create_ip()
        server = session.create_server(name, image, ip)
        session.perform_action(server, 'poweron')

        while server['state'] != 'running':
            sleep(1)
            server = session.get_server(server)
            now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            print(
                "\r%s\tServer %s is... %s" % (now, name, server['state']),
                end='')
            print(10 * ' ', end='', flush=True)

    except Exception:
        clean_up_server(session, server, ip)
        raise

    # Success!
    return server


def generate_hosts(ip_mappings):
    """Return contents for Ansible hosts file."""
    line_template = (
        '{host} '
        'ansible_host={ip} '
        'ansible_user=root ansible_'
        'ssh_private_key_file=.scaleway/scaleway'
        '\n')
    return ''.join(
        line_template.format(host=host, ip=ip)
        for ip, host in sorted(ip_mappings.items()))


def write_hosts(ips, path):
    """Write Ansible hosts file."""
    contents = generate_hosts(ips)
    with open(path, 'w', encoding='utf-8') as hosts:
        hosts.write(contents)


def attempt(action, nonfatal_errors):
    """Make one attempt at `action`, return whether it succeeded.

    :param action: Callable to be run.
    :param nonfatal_errors: Exception class or tuple of exception classes.
        If any of these occurs, don't propagate it, but return `False`.
    :return: `True` if `action` succeeded, or `False` on non-fatal error.
    """
    try:
        action()
        return True
    except nonfatal_errors:
        return False


def retry(action, nonfatal_errors=Exception, attempts=3, delay_seconds=10):
    """Try `action`, and retry if necessary.

    :param action: Callable to be (re)tried.
    :param nonfatal_errors: Exception class or tuple of exception classes
        which are acceptable.  If any of these occurs, we retry.
    :param attempts: How many attempts should be made, in total.
    :param delay_seconds: How many seconds to wait before retrying.
    """
    for _ in range(attempts - 1):
        if attempt(action, nonfatal_errors):
            # Success.
            return
        print("Failure!  Retrying after %s second(s)." % delay_seconds)
        sleep(delay_seconds)
    # Last attempt: don't swallow any errors, just let them bubble up.
    action()


def run_ansible(server_name, ansible_dir, playbook, ips):
    """Configure and run Ansible.

    :param server_name: Name of the server that's being set up.
    :param ansible_dir: Location on the local filesystem where the Ansible
        playbook is to be found, and where Ansible's `hosts` file can be
        written.  Ansible will be run from this directory.
    :param ips: Mapping of IP addresses to server names.
    """
    hosts_file = os.path.join(ansible_dir, 'hosts')
    requirements_file = os.path.join(ansible_dir, 'requirements.yml')
    playbook_file = os.path.join(ansible_dir, playbook)
    write_hosts(ips, hosts_file)
    check_call([
        'ansible-galaxy', 'install',
        '-r', requirements_file,
        ],
        cwd=ansible_dir)

    # Sometimes this fails because of a race condition.  We've seen this work
    # in the default playbook (which had a vars_prompt), but fail in the
    # monitoring.yml playbook (which was not delayed by the prompt).
    # So, on failure, simply wait and retry.
    run_playbook = lambda: check_call([
        'ansible-playbook',
        playbook_file,
        '-i', hosts_file,
        '-l', server_name,
        ],
        cwd=ansible_dir)
    # TODO: Narrow down nonfatal exception type(s).
    retry(run_playbook)


def list_server_names(session, glob):
    """Return list of matching server names."""
    return [
        server['name']
        for server in session.list_servers(glob)
        ]


def suffix_name(base_name, existing_names):
    """Add unique suffix to `base_name`."""
    # TODO: There must be a better way to do this.
    existing_names = frozenset(existing_names)
    for suffix in range(99):
        new_name = '{base_name}-{suffix}'.format_map(vars())
        if new_name not in existing_names:
            return new_name

    raise Exception("Too many servers with base name '%s'." % base_name)


def check_server_clash(args, session):
    """Deal with clashing server names.  Return prevailing name."""
    shorthand = {
        'f': 'fail',
        'i': 'ignore',
        'r': 'rename',
        }
    # Translate 'clash' argument to its long, canonical form.
    response = args.clash
    response = shorthand.get(response, response)
    assert response in shorthand.values()

    name = args.server_name
    if response == 'ignore':
        # Always OK.
        return name

    existing_servers = list_server_names(session, name + '*')
    if name not in existing_servers:
        # This name is unique.
        return name

    if response == 'fail':
        raise Exception("Server '%s' already exists." % name)

    assert response == 'rename'
    return suffix_name(name, existing_servers)


def create_server(args):
    """Subcommand: create server."""
    session = ScalewaySession(
        service_url=SERVICE_URL, api_token=args.api_token,
        organization=args.organization)

    name = check_server_clash(args, session)
    server = make_server(session, name, args.image)
    debug_output("Server is running:", server['id'])

    # Map IP addresses to server names.
    ips = {
        ip['address']: ip['server']['name']
        for ip in session.list_ips()
        if ip.get('server') is not None
        }
    run_ansible(name, args.ansible_dir, args.ansible_playbook, ips)

    server_info = {
        'id': server['id'],
        'name': server['name'],
        'public_ip': server['public_ip']['address'],
        'private_ip': server['private_ip']
    }
    debug_output("Server is provisioned:", server_info)


def clean_up_server(session, server, ip=None):
    """Destroy server, optionally deallocate public IP."""
    if server is not None:
        # TODO: 404 is OK here -- means the server's already gone.
        session.perform_action(server, 'terminate')
    if ip is not None:
        session.delete_ip(ip)


def matches_glob(server, glob):
    """Does `server` match `glob` in its name or ID?"""
    for key in ('id', 'name'):
        if fnmatch(server[key], glob):
            return True
    return False


def delete_server(args):
    """Subcommand: destroy server."""
    session = ScalewaySession(
        service_url=SERVICE_URL, api_token=args.api_token)
    # TODO: Can we do this in a single bulk request?
    for server in session.list_servers(args.server_pattern):
        session.delete_server(server['id'])


def main():
    args = parse_args()
    args.func(args)


if __name__ == '__main__':
    main()
