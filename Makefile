#! /usr/bin/env make


# Check for Python lint.  Requires pocketlint.
pylint:
	find . -name \*.py | xargs pocketlint


# Run Python tests.  Requires python3-pytest.
pytest:
	py.test-3
