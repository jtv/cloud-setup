#Eonics CDaaS#

=== dot

digraph G {
  edge[label=depends_on]
  jenkins -> nexus 
  jenkins -> gitlab
  jenkins -> sonar
  sonar -> sonardb

  edge[label="links" arrowhead=empty]
  "reverse-proxy" -> sonar
  "reverse-proxy" -> jenkins
  "reverse-proxy" -> nexus
  "reverse-proxy" -> gitlab
  gateway -> keycloak
  gateway -> "keycloak-proxy"
}

===

## Test
