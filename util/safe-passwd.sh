#!/usr/bin/env bash

passwd=$(pwgen -N 1 12);
echo ${passwd:0:3}-${passwd:3:3}-${passwd:6:3}-${passwd:9:3};
passwd=
