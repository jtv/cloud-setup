<#import "template.ftl" as layout>
<@layout.registrationLayout; section>
    <#if section = "title">
        ${msg("registerWithTitle",(realm.displayName!''))}
    <#elseif section = "header">
        ${msg("registerWithTitleHtml",(realm.displayNameHtml!''))}
    <#elseif section = "form">
	
        <form id="kc-register-form" class="${properties.kcFormClass!}" action="${url.registrationAction}" method="post">
          <input type="text" readonly value="this is not a login form" style="display: none;">
          <input type="password" readonly value="this is not a login form" style="display: none;">

          <#if !realm.registrationEmailAsUsername>
            <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('username',properties.kcFormGroupErrorClass!)}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="username" class="${properties.kcLabelClass!}">${msg("username")}</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="text" id="username" class="${properties.kcInputClass!}" name="username" value="${(register.formData.username!'')?html}" />
                </div>
            </div>
          </#if>
            <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('firstName',properties.kcFormGroupErrorClass!)}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="firstName" class="${properties.kcLabelClass!}">${msg("firstName")}</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="text" id="firstName" class="${properties.kcInputClass!}" name="firstName" value="${(register.formData.firstName!'')?html}" />
                </div>
            </div>

            <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('lastName',properties.kcFormGroupErrorClass!)}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="lastName" class="${properties.kcLabelClass!}">${msg("lastName")}</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="text" id="lastName" class="${properties.kcInputClass!}" name="lastName" value="${(register.formData.lastName!'')?html}" />
                </div>
            </div>

            <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('email',properties.kcFormGroupErrorClass!)}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="email" class="${properties.kcLabelClass!}">${msg("email")}</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="text" id="email" class="${properties.kcInputClass!}" name="email" value="${(register.formData.email!'')?html}" />
                </div>
            </div>
            
            <div class="${properties.kcFormGroupClass!}">
	   <div class="${properties.kcLabelWrapperClass!}">
	       <label for="user.attributes.telephone_number" class="control-label">Telefoonnummer</label>
	   </div>

	   <div class="${properties.kcInputWrapperClass!}">
	       <input type="text" class="form-control" id="user.attributes.telephone_number" name="user.attributes.telephone_number" value="${(register.formData['user.attributes.telephone_number']!'')?html}"/>
	   </div>
	</div>

            <div class="form-group">
	   <div class="${properties.kcLabelWrapperClass!}">
	       <label for="user.attributes.linkedinurl" class="control-label">Linkedin URL</label>
	   </div>

	   <div class="${properties.kcInputWrapperClass!}">
	       <input type="text" class="form-control" id="user.attributes.linkedinurl" 
name="user.attributes.linkedinurl" value="${(register.formData['user.attributes.linkedinurl']!'')?html}"/>
	   </div>
	</div>

        <div class="form-group" id="via_selection">
	   <div class="${properties.kcLabelWrapperClass!}">
	       <label for="user.attributes.user_type" class="control-label">Hoe heb je ons gevonden?</label>
	   </div>

	   <div class="${properties.kcInputWrapperClass!}">
               <select name="via" onChange="setVia(this.value)">
                    <option id="via.none" value="none">-</option>
                    <option id="via.linkedin" value="linkedin">LinkedIn</option>
                    <option id="via.facebook" value="facebook">Facebook</option>
                    <option id="via.google" value="google">Google</option>
                    <option id="via.hessel" value="hessel">Attentie Hessel van Tuinen</option>
               </select>
               <input                        
                     type="text" class="form-control" id="user.attributes.via" 
                     name="user.attributes.via" value="${(register.formData['user.attributes.via']!'')?html}" style="display:none" />
            
            </div>
	</div>

        <div class="form-group">
	   <div class="${properties.kcLabelWrapperClass!}">
	       <label for="user.attributes.user_type" class="control-label">In welke rol maak je gebruik van Observ?</label>
	   </div>

	   <div class="${properties.kcInputWrapperClass!}">
               
	       <input type="radio" id="user_type.freelancer" class="form-radio" 
			 value="fl" name="user_type" onClick="setSelectedUserType(this.value)" /> Freelancer 
               <br /><input type="radio" id="user_type.freelancer_to_be" class="form-radio" 
			 value="fl2b" name="user_type" onClick="setSelectedUserType(this.value)" /> Freelancer in spe (momenteel nog in loondienst)
               <br /><input type="radio" id="user_type.recruiter" class="form-radio" 
			 value="r" name="user_type" onClick="setSelectedUserType(this.value)" /> Recruiter
               <br /><input type="radio" id="user_type.consultancy" class="form-radio" 
			 value="c" name="user_type" onClick="setSelectedUserType(this.value)" /> Staffing-medewerker consultancy- of detacheringsbureau
               <br /><input type="radio" id="user_type.opdrachtgever" class="form-radio" 
			 value="o" name="user_type" onClick="setSelectedUserType(this.value)" /> Opdrachtgever
               <br /><input type="radio" id="user_type.other" class="form-radio" 
			 value="a" name="user_type" onClick="setSelectedUserType(this.value)" /> Anders, namelijk
               <input type="text" onClick="document.getElementById('user_type.other').checked = true);" 
style="width:200px;" id="user_type_other" name="user.attributes.user_type_other" 
value="${(register.formData['user.attributes.user_type_other']!'')?html}"/>        
               <input                        
                     type="text" class="form-control" id="user.attributes.user_type" 
                     name="user.attributes.user_type" value="${(register.formData['user.attributes.user_type']!'')?html}" style="display:none" />
            
            </div>
	</div>

        <div class="form-group" id="company_block" style="display:none">
	   <div class="${properties.kcLabelWrapperClass!}">
	       <label for="user.attributes.company" class="control-label">Bedrijfsnaam</label>
	   </div>

	   <div class="${properties.kcInputWrapperClass!}">
	       <input type="text" class="form-control" id="user.attributes.company" name="user.attributes.company" 
value="${(register.formData['user.attributes.company']!'')?html}"/>
	   </div>
	</div>

        <div id="freelancer_block" style="display:none">
	<div class="form-group">
	   <div class="${properties.kcLabelWrapperClass!}">
		    <label id="is_available_fl" for="is_available" class="control-label">Ben je binnenkort beschikbaar?</label>
                    <label id="is_available_c" for="is_available" class="control-label" style="display:none">Zijn er momenteel professionals beschikbaar in jullie organisatie?</label>
		</div>
		<div class="${properties.kcInputWrapperClass!}">
	   	    <input type="radio" id="user.attributes.is_available.yes" class="form-radio" 
			 value="1" onClick="toggleAvailability(this.checked)" name="available" /> Ja
		    <input type="radio" id="user.attributes.is_available.no" class="form-radio" 
			 value="0" onClick="toggleAvailability(!this.checked)" name="available" /> Nee
		</div>
                    
                    <input 
                        type="text" class="form-control" id="user.attributes.is_available" 
name="user.attributes.is_available" value="${(register.formData['user.attributes.is_available']!'')?html}" style="display:none"/>
                
                    
	</div>
        <div id="available_block" style="display:none">
	<div class="form-group" >
	   <div class="${properties.kcLabelWrapperClass!}">
		    <label for="recruiter_accessible" class="control-label">Wil je hiervoor benaderd worden door opdrachtgevers
en recruiters op het Observ platform?</label>
		</div>
		<div class="${properties.kcInputWrapperClass!}" >
	   	    <input type="radio" id="user.attributes.recruiter_accessible.yes" class="form-radio" 
			 onClick="toggleRecruiterAccess(this.checked)" name="recruiter_accessible" /> Ja
		    <input type="radio" id="user.attributes.recruiter_accessible.no" class="form-radio" 
			 onClick="toggleRecruiterAccess(!this.checked)" name="recruiter_accessible" /> Nee
		</div>
                    <input
                        type="text" class="form-control" id="user.attributes.recruiter_accessible" 
name="user.attributes.recruiter_accessible" value="${(register.formData['user.attributes.recruiter_accessible']!'')?html}" style="display:none"/>
                
	</div>

        <div id="recruiter_access_block" style="display:none">

        <div class="form-group" >
	   <div class="${properties.kcLabelWrapperClass!}">
	       <label for="user.attributes.availability_date" class="control-label">Per wanneer ben je beschikbaar?</label>
	   </div>

	   <div class="${properties.kcInputWrapperClass!}">
	       <input type="text" class="form-control" id="user.attributes.availability_date" name="user.attributes.availability_date"
 value="${(register.formData['user.attributes.availability_date']!'')?html}"/>
	   </div>
	</div>
        
	<div class="form-group">
	   <div class="${properties.kcLabelWrapperClass!}">
	       <label for="user.attributes.workarea" class="control-label">Werkgebied</label>
	   </div>

	   <div class="${properties.kcInputWrapperClass!}">
	       <input type="text" class="form-control" id="user.attributes.workarea" 
placeholder="bijv. 'Omgeving Amsterdam', 'Randstad' of 'Nijmegen + 45 min'" name="user.attributes.workarea" 
                value="${(register.formData['user.attributes.workarea']!'')?html}"/>
	   </div>

	</div>


	<div class="form-group">
	   <div class="${properties.kcLabelWrapperClass!}">
	       <label for="user.attributes.required_role" class="control-label">In welke rol zoek je een opdracht?</label>
	   </div>

	   <div class="${properties.kcInputWrapperClass!}">
	       <input type="text" class="form-control" id="user.attributes.required_role" name="user.attributes.required_role" 
value="${(register.formData['user.attributes.required_role']!'')?html}"/>
	   </div>
	</div>


	<div class="form-group">
	   <div class="${properties.kcLabelWrapperClass!}">
	       <label for="user.attributes.required_rate" class="control-label">Wat is je minimale tarief?</label>
	   </div>

	   <div class="${properties.kcInputWrapperClass!}">
	       <input type="text" class="form-control" id="user.attributes.required_rate" name="user.attributes.required_rate" 
value="${(register.formData['user.attributes.required_rate']!'')?html}"/>
	   </div>
	</div>


	
        </div>
        </div>
        </div>

        <div id="keywords_block" class="form-group">
	   <div class="${properties.kcLabelWrapperClass!}">
	       <label id="keywords_label_fl" for="user.attributes.keywords" class="control-label">Geef in steekwoorden aan wat voor opdracht je zoekt</label>
               <label id="keywords_label_c" for="user.attributes.keywords" class="control-label" style="display:none">Geef in steekwoorden aan wat voor opdracht(en) je zoekt
                voor de beschikbare professional(s)</label>
           </div>

	   <div class="${properties.kcInputWrapperClass!}">
                <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
                <script>tinymce.init({ selector:'textarea' });</script>
                <textarea id="user.attributes.keywords" name="user.attributes.keywords" 
placeholder="bijv. Scrum, groot zakelijk, niet bij een bank, full-stack Angular / Java" cols="96" rows="10">
${(register.formData['user.attributes.keywords']!'')?html}</textarea>
	   </div>
	</div>
        <div>&nbsp;</div>
       <script>
            var raInput = document.getElementById('user.attributes.recruiter_accessible');
            var aiInput = document.getElementById('user.attributes.is_available');
            var utInput = document.getElementById('user.attributes.user_type');

            setSelectedUserType(utInput.value);
            setVia(document.getElementById('user.attributes.via').value);
            var viaFromQs = getQueryStringParam('via');
            var viaFromCookie = getCookie('via');

            if(viaFromCookie != null && viaFromCookie != '') {
                hideViaBox();
                setVia(viaFromCookie);
            }

            if(viaFromQs != null && viaFromQs != '') {
                hideViaBox();
                setCookie('via',viaFromQs);
                setVia(viaFromQs);
            }
            toggleAvailability(aiInput.value == 1);
            toggleRecruiterAccess(raInput.value == 1);  
            
            function hideViaBox() {
                document.getElementById('via_selection').style.display = 'none';
            }   
            
            function getQueryStringParam ( field, url ) {
                var href = url ? url : window.location.href;
                var reg = new RegExp( '[?&]' + field + '=([^&#]*)', 'i' );
                var string = reg.exec(href);
                return string ? string[1] : null;
            }

            function setCookie(cname, cvalue) {
                document.cookie = cname + "=" + cvalue;
            }

            function getCookie(cname) {
                var name = cname + "=";
                var ca = document.cookie.split(';');
                for(var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ') {
                        c = c.substring(1);
                    }
                    if (c.indexOf(name) == 0) {
                        return c.substring(name.length, c.length);
                    }
                }
                return "";
            }

            function toggleRecruiterAccess(recruiterAccess) {
                if(recruiterAccess) {
                    document.getElementById('user.attributes.recruiter_accessible.yes').checked = true;
                    if(getUserType() == 'fl') {
                        document.getElementById('recruiter_access_block').style.display = 'block';
                    }
                    raInput.value = 1;
                    toggleKeywordsBlock(true);
                } else {
                    document.getElementById('user.attributes.recruiter_accessible.no').checked = true;
                    document.getElementById('recruiter_access_block').style.display = 'none';
                    raInput.value = 0;
                    toggleKeywordsBlock(false);
                }
            }

            function toggleKeywordsBlock(keywordsBlock) {
                if(keywordsBlock) {
                    document.getElementById('keywords_block').style.display = 'block';
                    if(getUserType() == 'fl') {
                        document.getElementById('keywords_label_fl').style.display = 'block';
                        document.getElementById('keywords_label_c').style.display = 'none';
                    } else if(getUserType() == 'c') {
                        document.getElementById('keywords_label_fl').style.display = 'none';
                        document.getElementById('keywords_label_c').style.display = 'block';
                    }
                } else {
                    document.getElementById('keywords_block').style.display = 'none';
                }
            }

            function toggleAvailability(available) {
                if(available) {
                    document.getElementById('user.attributes.is_available.yes').checked = true;
                    document.getElementById('available_block').style.display = 'block';
                    aiInput.value = 1;
                } else {
                    document.getElementById('user.attributes.is_available.no').checked = true;
                    document.getElementById('available_block').style.display = 'none';
                    aiInput.value = 0;
                    toggleRecruiterAccess(false);
                }
            }

            function toggleFreelancer(freelancer) {
                if(freelancer) {
                    document.getElementById('freelancer_block').style.display = 'block';
                    if(getUserType() == 'fl') {
                        document.getElementById('is_available_fl').style.display = 'block';
                        document.getElementById('is_available_c').style.display = 'none';
                    } else if(getUserType() == 'c') {
                        document.getElementById('is_available_fl').style.display = 'none';
                        document.getElementById('is_available_c').style.display = 'block';
                    }
                } else {
                    document.getElementById('freelancer_block').style.display = 'none';
                    toggleAvailability(false);
                }
            }

            function toggleCompany(company) {
                if(company) {
                    document.getElementById('company_block').style.display = 'block';
                } else {                            
                    document.getElementById('company_block').style.display = 'none';
                }
            }

            function setVia(v) {
                if(v != null && v != '') {
                    var viaOption = document.getElementById('via.' + v);
                    if(viaOption != null) {
                        viaOption.selected = true;
                    }
                    document.getElementById('user.attributes.via').value = v;
                }
            }

            function setSelectedUserType(v) {
                var userTypeChanged = (v != getUserType()); 
                setUserType(v);
                if(v == 'fl') {
                    document.getElementById('user_type.freelancer').checked = true;
                    toggleCompany(true);
                    if(userTypeChanged) {
                        toggleFreelancer(false);
                    }
                    toggleFreelancer(true);
                } else if (v == 'fl2b') {
                    document.getElementById('user_type.freelancer_to_be').checked = true;
                    toggleCompany(false);
                    toggleFreelancer(false);
                } else if (v == 'r') {
                    document.getElementById('user_type.recruiter').checked = true;
                    toggleCompany(true);
                    toggleFreelancer(false);
                } else if (v == 'c') {
                    document.getElementById('user_type.consultancy').checked = true;
                    toggleCompany(true);
                    if(userTypeChanged) {
                        toggleFreelancer(false);
                    }
                    toggleFreelancer(true);
                } else if (v == 'o') {
                    document.getElementById('user_type.opdrachtgever').checked = true;
                    toggleCompany(true);
                    toggleFreelancer(false);
                } else if (v == 'a') {
                    document.getElementById('user_type.other').checked = true;
                    toggleCompany(false);
                    toggleFreelancer(false);
                }
            }

            function setUserType(v) {                        
                document.getElementById('user.attributes.user_type').value = v;
            }

            function getUserType() {                        
                return document.getElementById('user.attributes.user_type').value;
            }
        </script>

            <#if passwordRequired>
            <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('password',properties.kcFormGroupErrorClass!)}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="password" class="${properties.kcLabelClass!}">${msg("password")}</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="password" id="password" class="${properties.kcInputClass!}" name="password" />
                </div>
            </div>

            <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('password-confirm',properties.kcFormGroupErrorClass!)}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="password-confirm" class="${properties.kcLabelClass!}">${msg("passwordConfirm")}</label>
                </div>
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="password" id="password-confirm" class="${properties.kcInputClass!}" name="password-confirm" />
                </div>
            </div>
            </#if>

 	    <#if recaptchaRequired??>
            <div class="form-group">
                <div class="${properties.kcInputWrapperClass!}">
                    <div class="g-recaptcha" data-size="compact" data-sitekey="${recaptchaSiteKey}"></div>
                </div>
            </div>
            </#if>

            <div class="${properties.kcFormGroupClass!}">
                <div id="kc-form-options" class="${properties.kcFormOptionsClass!}">
                    <div class="${properties.kcFormOptionsWrapperClass!}">
                        <span><a href="${url.loginUrl}">${msg("backToLogin")}</a></span>
                    </div>
                </div>

                <div id="kc-form-buttons" class="${properties.kcFormButtonsClass!}">
                    <input class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonLargeClass!}" type="submit" value="${msg("doRegister")}"/>
                </div>
            </div>
        </form>
    </#if>
</@layout.registrationLayout>
