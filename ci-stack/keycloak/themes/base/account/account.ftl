<#import "template.ftl" as layout>
<@layout.mainLayout active='account' bodyClass='user'; section>

    <div class="row">
        <div class="col-md-10">
            <h2>${msg("editAccountHtmlTtile")}</h2>
        </div>
        <div class="col-md-2 subtitle">
            <span class="subtitle"><span class="required">*</span> ${msg("requiredFields")}</span>
        </div>
    </div>

    <form action="${url.accountUrl}" class="form-horizontal" method="post">

        <input type="hidden" id="stateChecker" name="stateChecker" value="${stateChecker?html}">

        <div class="form-group ${messagesPerField.printIfExists('username','has-error')}">
            <div class="col-sm-2 col-md-2">
                <label for="username" class="control-label">${msg("username")}</label> <#if realm.editUsernameAllowed><span class="required">*</span></#if>
            </div>

            <div class="col-sm-10 col-md-10">
                <input type="text" class="form-control" id="username" name="username" <#if !realm.editUsernameAllowed>disabled="disabled"</#if> value="${(account.username!'')?html}"/>
            </div>
        </div>

        <div class="form-group ${messagesPerField.printIfExists('email','has-error')}">
            <div class="col-sm-2 col-md-2">
            <label for="email" class="control-label">${msg("email")}</label> <span class="required">*</span>
            </div>

            <div class="col-sm-10 col-md-10">
                <input type="text" class="form-control" id="email" name="email" autofocus value="${(account.email!'')?html}"/>
            </div>
        </div>

        <div class="form-group ${messagesPerField.printIfExists('firstName','has-error')}">
            <div class="col-sm-2 col-md-2">
                <label for="firstName" class="control-label">${msg("firstName")}</label> <span class="required">*</span>
            </div>

            <div class="col-sm-10 col-md-10">
                <input type="text" class="form-control" id="firstName" name="firstName" value="${(account.firstName!'')?html}"/>
            </div>
        </div>

        <div class="form-group ${messagesPerField.printIfExists('lastName','has-error')}">
            <div class="col-sm-2 col-md-2">
                <label for="lastName" class="control-label">${msg("lastName")}</label> <span class="required">*</span>
            </div>

            <div class="col-sm-10 col-md-10">
                <input type="text" class="form-control" id="lastName" name="lastName" value="${(account.lastName!'')?html}"/>
            </div>
        </div>

        <div class="form-group">
	   <div class="col-sm-2 col-md-2">
	       <label for="user.attributes.telephone_number" class="control-label">Telefoonnummer</label>
	   </div>

	   <div class="col-sm-10 col-md-10">
	       <input type="text" class="form-control" id="user.attributes.telephone_number" name="user.attributes.telephone_number" value="${(account.attributes.telephone_number!'')?html}"/>
	   </div>
	</div>

	<div class="form-group">
	   <div class="col-sm-2 col-md-2">
	       <label for="user.attributes.linkedinurl" class="control-label">Linkedin URL</label>
	   </div>

	   <div class="col-sm-10 col-md-10">
	       <input type="text" class="form-control" id="user.attributes.linkedinurl" name="user.attributes.linkedinurl" value="${(account.attributes.linkedinurl!'')?html}"/>
	   </div>
	</div>

        <div class="form-group">
	   <div class="col-sm-2 col-md-2">
	       <label for="user.attributes.user_type" class="control-label">In welke rol maak je gebruik van Observ?</label>
	   </div>

	   <div class="col-sm-10 col-md-10">
               
	       <input type="radio" id="user_type.freelancer" class="form-radio" 
			 value="fl" name="user_type" onClick="setSelectedUserType(this.value)"  /> Freelancer 
               <br /><input type="radio" id="user_type.freelancer_to_be" class="form-radio" 
			 value="fl2b" name="user_type" onClick="setSelectedUserType(this.value)" /> Freelancer in spe (momenteel nog in loondienst)
               <br /><input type="radio" id="user_type.recruiter" class="form-radio" 
			 value="r" name="user_type" onClick="setSelectedUserType(this.value)" /> Recruiter
               <br /><input type="radio" id="user_type.consultancy" class="form-radio" 
			 value="c" name="user_type" onClick="setSelectedUserType(this.value)" /> Staffing-medewerker consultancy- of detacheringsbureau
               <br /><input type="radio" id="user_type.opdrachtgever" class="form-radio" 
			 value="o" name="user_type" onClick="setSelectedUserType(this.value)" /> Opdrachtgever
               <br /><input type="radio" id="user_type.other" class="form-radio" 
			 value="a" name="user_type" onClick="setSelectedUserType(this.value)" /> Anders, namelijk
               <input type="text" onClick="document.getElementById('user_type.other').checked = true);" style="width:200px;" id="user_type_other" name="user.attributes.user_type_other" value="${(account.attributes.user_type_other!'')?html}"/>        
               <input                        
                     type="text" class="form-control" id="user.attributes.user_type" 
                     name="user.attributes.user_type" value="${(account.attributes.user_type!'')?html}" style="display:none" />
            
            </div>
	</div>

        <div class="form-group" id="company_block" style="display:none">
	   <div class="col-sm-2 col-md-2">
	       <label for="user.attributes.company" class="control-label">Bedrijfsnaam</label>
	   </div>

	   <div class="col-sm-10 col-md-10">
	       <input type="text" class="form-control" id="user.attributes.company" name="user.attributes.company" value="${(account.attributes.company!'')?html}"/>
	   </div>
	</div>

        <div id="freelancer_block" style="display:none">
	<div class="form-group">
	   <div class="col-sm-2 col-md-2">
		    <label id="is_available_fl" for="is_available" class="control-label">Ben je binnenkort beschikbaar?</label>
                    <label id="is_available_c" for="is_available" class="control-label" style="display:none">Zijn er momenteel professionals beschikbaar in jullie organisatie?</label>
		</div>
		<div class="col-sm-10 col-md-10">
	   	    <input type="radio" id="user.attributes.is_available.yes" class="form-radio" 
			 value="1" onClick="toggleAvailability(this.checked)" name="available" /> Ja
		    <input type="radio" id="user.attributes.is_available.no" class="form-radio" 
			 value="0" onClick="toggleAvailability(!this.checked)" name="available" /> Nee
		</div>
                <div>
                    
                    <input 
                        type="text" class="form-control" id="user.attributes.is_available" 
name="user.attributes.is_available" value="${(account.attributes.is_available!'')?html}" style="display:none"/>
                
                    
                </div>
	</div>
        <div id="available_block" style="display:none">
	<div class="form-group" >
	   <div class="col-sm-2 col-md-2">
		    <label for="recruiter_accessible" class="control-label">Wil je hiervoor benaderd worden door opdrachtgevers
en recruiters op het Observ platform?</label>
		</div>
		<div class="col-sm-10 col-md-10" >
	   	    <input type="radio" id="user.attributes.recruiter_accessible.yes" class="form-radio" 
			 onClick="toggleRecruiterAccess(this.checked)" name="recruiter_accessible" /> Ja
		    <input type="radio" id="user.attributes.recruiter_accessible.no" class="form-radio" 
			 onClick="toggleRecruiterAccess(!this.checked)" name="recruiter_accessible" /> Nee
		</div>
                <div>
                    <input
                        type="text" class="form-control" id="user.attributes.recruiter_accessible" 
name="user.attributes.recruiter_accessible" value="${(account.attributes.recruiter_accessible!'')?html}" style="display:none"/>
                
                </div>
	</div>

        <div id="recruiter_access_block" style="display:none">

        <div class="form-group" >
	   <div class="col-sm-2 col-md-2">
	       <label for="user.attributes.availability_date" class="control-label">Per wanneer ben je beschikbaar?</label>
	   </div>

	   <div class="col-sm-10 col-md-10">
	       <input type="text" class="form-control" id="user.attributes.availability_date" name="user.attributes.availability_date" value="${(account.attributes.availability_date!'')?html}"/>
	   </div>
	</div>
        
	<div class="form-group">
	   <div class="col-sm-2 col-md-2">
	       <label for="user.attributes.workarea" class="control-label">Werkgebied</label>
	   </div>

	   <div class="col-sm-10 col-md-10">
	       <input type="text" class="form-control" id="user.attributes.workarea" 
placeholder="bijv. 'Omgeving Amsterdam', 'Randstad' of 'Nijmegen + 45 min'" name="user.attributes.workarea" value="${(account.attributes.workarea!'')?html}"/>
	   </div>

	</div>


	<div class="form-group">
	   <div class="col-sm-2 col-md-2">
	       <label for="user.attributes.required_role" class="control-label">In welke rol zoek je een opdracht?</label>
	   </div>

	   <div class="col-sm-10 col-md-10">
	       <input type="text" class="form-control" id="user.attributes.required_role" name="user.attributes.required_role" value="${(account.attributes.required_role!'')?html}"/>
	   </div>
	</div>


	<div class="form-group">
	   <div class="col-sm-2 col-md-2">
	       <label for="user.attributes.required_rate" class="control-label">Wat is je minimale tarief?</label>
	   </div>

	   <div class="col-sm-10 col-md-10">
	       <input type="text" class="form-control" id="user.attributes.required_rate" name="user.attributes.required_rate" value="${(account.attributes.required_rate!'')?html}"/>
	   </div>
	</div>

        </div>
        </div>
        </div>

        <div id="keywords_block" class="form-group" style="display:none">
	   <div class="col-sm-2 col-md-2">
	       <label id="keywords_label_fl" for="user.attributes.keywords" class="control-label">Geef in steekwoorden aan wat voor opdracht je zoekt</label>
               <label id="keywords_label_c" for="user.attributes.keywords" class="control-label" style="display:none">Geef in steekwoorden aan wat voor opdracht(en) je zoekt
                voor de beschikbare professional(s)</label>
	   </div>

	   <div class="col-sm-10 col-md-10">
                <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
                <script>tinymce.init({ selector:'textarea' });</script>
                <textarea id="user.attributes.keywords" name="user.attributes.keywords" 
placeholder="bijv. Scrum, groot zakelijk, niet bij een bank, full-stack Angular / Java" cols="101" rows="10">
${(account.attributes.keywords!'')?html}</textarea>
	   </div>
	</div>

        <script>
            var raInput = document.getElementById('user.attributes.recruiter_accessible');
            var aiInput = document.getElementById('user.attributes.is_available');
            var utInput = document.getElementById('user.attributes.user_type');

            setSelectedUserType(utInput.value);
            toggleAvailability(aiInput.value == 1);
            toggleRecruiterAccess(raInput.value == 1);  

            function toggleRecruiterAccess(recruiterAccess) {
                if(recruiterAccess) {
                    document.getElementById('user.attributes.recruiter_accessible.yes').checked = true;
                    if(getUserType() == 'fl') {
                        document.getElementById('recruiter_access_block').style.display = 'block';
                    }
                    raInput.value = 1;
                    toggleKeywordsBlock(true);
                } else {
                    document.getElementById('user.attributes.recruiter_accessible.no').checked = true;
                    document.getElementById('recruiter_access_block').style.display = 'none';
                    raInput.value = 0;
                    toggleKeywordsBlock(false);
                }
            }

            function toggleKeywordsBlock(keywordsBlock) {
                if(keywordsBlock) {
                    document.getElementById('keywords_block').style.display = 'block';
                    if(getUserType() == 'fl') {
                        document.getElementById('keywords_label_fl').style.display = 'block';
                        document.getElementById('keywords_label_c').style.display = 'none';
                    } else if(getUserType() == 'c') {
                        document.getElementById('keywords_label_fl').style.display = 'none';
                        document.getElementById('keywords_label_c').style.display = 'block';
                    }
                } else {
                    document.getElementById('keywords_block').style.display = 'none';
                }
            }

            function toggleAvailability(available) {
                if(available) {
                    document.getElementById('user.attributes.is_available.yes').checked = true;
                    document.getElementById('available_block').style.display = 'block';
                    aiInput.value = 1;
                } else {
                    document.getElementById('user.attributes.is_available.no').checked = true;
                    document.getElementById('available_block').style.display = 'none';
                    aiInput.value = 0;
                    toggleRecruiterAccess(false);
                }
            }

            function toggleFreelancer(freelancer) {
                if(freelancer) {
                    document.getElementById('freelancer_block').style.display = 'block';
                    if(getUserType() == 'fl') {
                        document.getElementById('is_available_fl').style.display = 'block';
                        document.getElementById('is_available_c').style.display = 'none';
                    } else if(getUserType() == 'c') {
                        document.getElementById('is_available_fl').style.display = 'none';
                        document.getElementById('is_available_c').style.display = 'block';
                    }
                } else {
                    document.getElementById('freelancer_block').style.display = 'none';
                    toggleAvailability(false);
                }
            }

            function toggleCompany(company) {
                if(company) {
                    document.getElementById('company_block').style.display = 'block';
                } else {                            
                    document.getElementById('company_block').style.display = 'none';
                }
            }

            function setSelectedUserType(v) {
                var userTypeChanged = (v != getUserType()); 
                setUserType(v);
                if(v == 'fl') {
                    document.getElementById('user_type.freelancer').checked = true;
                    toggleCompany(true);
                    if(userTypeChanged) {
                        toggleFreelancer(false);
                    }
                    toggleFreelancer(true);
                } else if (v == 'fl2b') {
                    document.getElementById('user_type.freelancer_to_be').checked = true;
                    toggleCompany(false);
                    toggleFreelancer(false);
                } else if (v == 'r') {
                    document.getElementById('user_type.recruiter').checked = true;
                    toggleCompany(true);
                    toggleFreelancer(false);
                } else if (v == 'c') {
                    document.getElementById('user_type.consultancy').checked = true;
                    toggleCompany(true);
                    if(userTypeChanged) {
                        toggleFreelancer(false);
                    }
                    toggleFreelancer(true);
                } else if (v == 'o') {
                    document.getElementById('user_type.opdrachtgever').checked = true;
                    toggleCompany(true);
                    toggleFreelancer(false);
                } else if (v == 'a') {
                    document.getElementById('user_type.other').checked = true;
                    toggleCompany(false);
                    toggleFreelancer(false);
                }
            }

            function setUserType(v) {                        
                document.getElementById('user.attributes.user_type').value = v;
            }

            function getUserType() {                        
                return document.getElementById('user.attributes.user_type').value;
            }
        </script>

        <div class="form-group">
            <div id="kc-form-buttons" class="col-md-offset-2 col-md-10 submit">
                <div class="">
                    <#if url.referrerURI??><a href="${url.referrerURI}">${msg("backToApplication")}/a></#if>
                    <button type="submit" class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonLargeClass!}" name="submitAction" value="Save">${msg("doSave")}</button>
                    <button type="submit" class="${properties.kcButtonClass!} ${properties.kcButtonDefaultClass!} ${properties.kcButtonLargeClass!}" name="submitAction" value="Cancel">${msg("doCancel")}</button>
                </div>
            </div>
        </div>
    </form>

</@layout.mainLayout>
