def exec() {
  sshagent(credentials: ['git-credentials']) {
    sh """
        git config user.email "noreply@jenkins.ictu"
        git config user.name "jenkins"
        git add "./*pom.xml"
        git commit -m "[Jenkins Pipeline] Change POM versions to ${env.VERSION} "
        git tag -a ${env.VERSION} -m "[Jenkins Pipeline] Create tag ${env.VERSION}"
        git push origin ${env.VERSION}
        git push --tags
    """ }

    sh "mvn deploy -P push -f pom.xml -Dmaven.test.skip=true"

}

return this;
