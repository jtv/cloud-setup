def exec() {
  wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'XTerm']) {
    sh "mvn versions:set -DnewVersion=${env.VERSION} -DenerateBackupPoms=false"
    sh "mvn clean install -DskipTests=true"
  }
}

return this;
