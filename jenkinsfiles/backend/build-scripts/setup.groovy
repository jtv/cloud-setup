def exec() {
  checkout scm

  def rev = env.BUILD_NUMBER
  def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'
  def pomVersion = matcher ? matcher[0][1] : null;
  matcher = null; // Not serializable

  if(env.BRANCH_NAME == 'master') {
    env.VERSION = "${pomVersion}.${rev}"
  } else {
    env.VERSION = "${pomVersion}.${rev}-${env.BRANCH_NAME.take(10)}"
  }
}

return this;
