def exec() {
  ws {
    wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'XTerm']) {
      checkout scm
      sh "mvn -pl '!packaging' clean install sonar:sonar"
    }
  }
}

return this;
