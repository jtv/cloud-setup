def exec() {
  sh """
    if [ ! -e /tmp/OWASP-Dependency-Check-Data/data ]; then
      mkdir -p /tmp/OWASP-Dependency-Check-Data/data
      chmod -R 777 /tmp/OWASP-Dependency-Check-Data/data
    fi
    rm -rf ${pwd()}/owasp &&
    mkdir ${pwd()}/owasp &&
    docker pull embrasure/owasp-dependency-check
    docker run --rm \
     -v ${pwd()}:/tmp/src \
     -v ${pwd()}/owasp:/tmp/reports \
     -v /tmp/OWASP-Dependency-Check-Data/data:/tmp/dependency-check/data \
     -w /tmp/src \
     -w /tmp/reports \
     embrasure/owasp-dependency-check \
     --disableAssembly \
     --suppression /tmp/src/build-scripts/suppression.xml
     --disableArchive"""

   publishHTML([
           allowMissing: false,
           alwaysLinkToLastBuild: false,
           keepAll: false,
           reportDir: "${pwd()}/owasp",
           reportFiles: 'dependency-check-report.html',
           reportName: 'OWASP Dependency checker report'])

   archive (includes: "**/dependency-check-report.xml")
}

return this;
