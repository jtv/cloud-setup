#!/bin/bash
#############################################################
# Create an instance of an application definition in the docker
# dashboard
#############################################################

echo "======================"
echo "[PUSHING TO DASHBOARD]"
echo "======================"

echo -e "COMPOSE_FILE                     = $1"
echo -e "DASHBOARD                        = $2\n"
echo -e "APPLICATION_NAME                 = $3\n"
echo -e "APPLICATION_VERSION              = $4\n"

COMPOSE_FILE=$1
DASHBOARD=$2
APPLICATION_NAME=$3
APPLICATION_VERSION=$4

curl -H "Content-Type: text/plain" -H "api-key: aa2a1ccc9248b3a5a4277cbae8c537a4" --upload-file $COMPOSE_FILE "http://$DASHBOARD/api/v2/apps/$APPLICATION_NAME/${APPLICATION_VERSION}"
cat $COMPOSE_FILE