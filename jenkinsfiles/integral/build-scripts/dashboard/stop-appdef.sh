#!/bin/bash
#############################################################
# Stops an instance of a application definition on the docker
# dashboard
#############################################################
echo "========"
echo "[STOP]"
echo "========"

echo -e "Starting application definition with the following parameters: "
echo -e "DASHBOARD          = $1\n"
echo -e "INSTANCE_NAME      = $2\n"

DASHBOARD=$1
INSTANCE_NAME=$2

echo "[INFO] Stopping application instance ${INSTANCE_NAME}"
curl -sS -XDELETE "http://$DASHBOARD/api/v2/instances/${INSTANCE_NAME}"

echo ""
echo "========"
echo "[END STOP]"
echo "========"
