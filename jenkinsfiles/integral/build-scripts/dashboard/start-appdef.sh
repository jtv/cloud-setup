#!/bin/bash
#############################################################
# Start an instance of an application definition in the docker
# dashboard
#############################################################

echo -e "DASHBOARD                        = $1\n"
echo -e "APPLICATION_NAME                 = $2\n"
echo -e "APPLICATION_VERSION              = $3\n"
echo -e "INSTANCE_NAME                    = $4\n"
echo -e "PARAMETERS                       = $5\n"

DASHBOARD=$1
APPLICATION_NAME=$2
APPLICATION_VERSION=$3
INSTANCE_NAME=$4
PARAMETERS=$5

echo "======="
echo "[START]"
echo "======="

echo "[INFO] Starting application instance ${INSTANCE_NAME} | http://$DASHBOARD/api/v1/start-app/$APPLICATION_NAME/${APPLICATION_VERSION}/${INSTANCE_NAME}${PARAMETERS}"
curl -sS -XGET "http://$DASHBOARD/api/v1/start-app/$APPLICATION_NAME/${APPLICATION_VERSION}/${INSTANCE_NAME}${PARAMETERS}"
sleep 15

a=0
TIMEOUT=48
INSTANCE_STATUS=$(curl -sS -XGET http://$DASHBOARD/api/v1/state/${INSTANCE_NAME})
echo "\n[INFO] Waiting for instance ${INSTANCE_NAME} to start"
while [ "$a" -lt "$TIMEOUT" ]
do
        a=$(($a+1))
        if [[ $INSTANCE_STATUS == "active" ]]; then
                echo "[INFO] Instance $INSTANCE_NAME has successfully started!"
                break
        fi
        echo "[INFO] Still waiting for ${INSTANCE_NAME} ..."
        echo "[INFO] Current status is -> ${INSTANCE_STATUS}"
        INSTANCE_STATUS=$(curl -sS -XGET http://$DASHBOARD/api/v1/state/${INSTANCE_NAME})
        sleep 5
done

if [[ "$a" -eq "$TIMEOUT" ]]; then
                echo "[INFO] Instance ${INSTANCE_NAME} could not be started. Please contact a system administrator!"
                exit 110
fi

echo "waiting 60s in order to give the application to load"
sleep 60

echo ""
echo "========"
echo "[END]"
echo "========"
