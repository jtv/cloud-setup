def exec() {
  wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'XTerm']) {
    sh  """
          cd target/classes

          #docker-compose -f docker-compose.yml -f docker-compose.art.yml -p ${env.INSTANCE_NAME} config > docker-compose.final.yml
          #chmod +x ${env.SCRIPTS_LOCATION}/push-appdef.sh
          #${env.SCRIPTS_LOCATION}/push-appdef.sh docker-compose.final.yml ${env.DASHBOARD} ${env.APPLICATION_NAME} ${env.VERSION}

          docker-compose -f docker-compose.yml -f docker-compose.art.yml pull
          docker-compose -f docker-compose.yml -f docker-compose.art.yml -p ${env.INSTANCE_NAME} up -d

          www_host=\$(docker-compose -f docker-compose.yml -f docker-compose.art.yml -p ${env.INSTANCE_NAME} port www 80)
          www_port_80=\"\$(echo \$www_host | sed -e 's,^.*:,:,g' -e 's,.*:\\([0-9]*\\).*,\\1,g' -e 's,[^0-9],,g')\"
          app_jacoco_host=\$(docker-compose -f docker-compose.yml -f docker-compose.art.yml -p ${env.INSTANCE_NAME} port app 1025)
          app_jacoco_port=\"\$(echo \$app_jacoco_host | sed -e 's,^.*:,:,g' -e 's,.*:\\([0-9]*\\).*,\\1,g' -e 's,[^0-9],,g')\"

          echo \$www_port_80 > www_port_80
          echo \$app_jacoco_port > app_jacoco_port
                    
          cd ../../test
          docker run --rm -u 1000:1000 -v `pwd`:/src mkenney/npm npm install
      """

    env.WWW_PORT="${readFile("target/classes/www_port_80").split("\n")[0]}"
    env.JACOCO_PORT="${readFile("target/classes/app_jacoco_port").split("\n")[0]}"

    echo 'running on: http://build-slave-1.vib.ictu:' + env.WWW_PORT
    try { 
      sleep 40
      sh "docker run --rm -u \"1000:1000\" -v ${pwd()}/test:/work --link ${env.INSTANCE_NAME}_www_1 -e BASE_URL=\"http://${env.INSTANCE_NAME}_www_1\" -e DIRECT_CONNECT=true testx/protractor"
      
    } catch (Exception ex) {}
  }

  publishHTML([
          allowMissing: false,
          alwaysLinkToLastBuild: false,
          keepAll: false,
          reportDir: "test/testresults/html/",
          reportFiles: 'htmlReport.html',
          reportName: 'ART Report'])

  archive (includes: '*test/testresults/**/*')
  step([$class: 'JUnitResultArchiver', testResults: '**/junit.xml'])

  //createCoverageReport()
}

def createCoverageReport() {
  sshagent(credentials: ['git-credentials']) {
    sh '''
        rm -rf sources
        mkdir sources
        git clone git@www.gitlab.vib.ictu:vib/back-end.git sources
      '''
  }

  sh "mvn install -f ${pwd()}/pom.xml -Djacoco.host=localhost -Djacoco.port=${env.JACOCO_PORT} -Punpack-dependencies,code-coverage,dump-data,create-report -DskipTests -Dapp.version=${env.APP_VERSION == 'latest' ? 'RELEASE' : env.APP_VERSION}"
  publishHTML( [
          allowMissing: false,
          alwaysLinkToLastBuild: false,
          keepAll: false,
          reportDir: "${pwd()}/target/report",
          reportFiles: 'index.html',
          reportName: 'ART Coverage Report'])
}

def cleanup() {
  try {
    try {
      timeout(time:5, unit:'MINUTES') {
        input message: "Break down ART-environment (http://build-slave-1.vib.ictu:${env.WWW_PORT})?"
      }
    } catch(err) { 
        def user = err.getCauses()[0].getUser()
        if('SYSTEM' != user.toString()) { 
          throw err;
        }
    }
    
    wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'XTerm']) {
      sh '''
          cd target/classes
          docker-compose -f docker-compose.yml -f docker-compose.art.yml -p ${INSTANCE_NAME} logs
          docker-compose -f docker-compose.yml -f docker-compose.art.yml -p ${INSTANCE_NAME} down
          '''
    }
  } catch (err) {}
}

return this;
