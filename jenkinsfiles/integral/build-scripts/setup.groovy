
import groovy.json.JsonSlurperClassic

def exec() {

  checkout scm

  def rev = env.BUILD_NUMBER
  def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'
  def pomVersion = matcher ? matcher[0][1] : null;
  matcher = null; // Not serializable

  if(env.BRANCH_NAME == 'master') {
    env.VERSION = "${pomVersion}.${rev}"
  } else {
    env.VERSION = "${pomVersion}.${rev}-${env.BRANCH_NAME.take(10)}"
  }
  env.DASHBOARD='www.dashboard.vib.ictu'
  env.INSTANCE_NAME='vib' + env.VERSION.replaceAll("\\.", "").replaceAll("\\-", "").toLowerCase()
  env.APPLICATION_NAME='vib'
  env.SCRIPTS_LOCATION=pwd() + '/build-scripts/dashboard'
  env.APP_VERSION = 'latest'
  env.NGINX_VERSION = 'latest'
  env.FRONTEND_VERSION = 'latest'
  env.SOAPUI_MOCKSERVICE_VERSION = 'latest'

  try {
    timeout(time: 10, unit: 'SECONDS') { 
      requestVersions()
    }
  } catch(err) {}
}

def requestVersions() {
  def params = [:]

  // First get all versions
  appChoices = getChoices('vib')
  nginxChoices = getChoices('vib-nginx')
  frontendChoices = getChoices('vib-frontend')
  soapuiMockserviceChoices = getChoices('vib-soapui-mockservice')

    // Get the definitive version
  def userInput = input id: 'userInput', message: "Provide the version of the java-ee component",
    parameters: [appChoices, nginxChoices, frontendChoices, soapuiMockserviceChoices]

  echo ("UserInput " + userInput)

  env.APP_VERSION = userInput['vib']
  env.NGINX_VERSION = userInput['vib-nginx']
  env.FRONTEND_VERSION = userInput['vib-frontend']
  env.SOAPUI_MOCKSERVICE_VERSION = userInput['vib-soapui-mockservice']

  return params
}

@NonCPS
def jsonParse(def json) {
    new groovy.json.JsonSlurperClassic().parseText(json)
}

def getChoices(repo) {
  // Reset temp array to choose from in the interface
  versies = []
  tagList = "http://www.docker-registry.vib.ictu:5000/v2/${repo}/tags/list".toURL().text
  versions = jsonParse(tagList).tags
  for (version in versions) {
    if(env.BRANCH_NAME == 'master') {
      versies.add(version)
    } else if(version.contains(env.BRANCH_NAME.take(10))) {
      versies.add(version)
    }
  }
  return new hudson.model.ChoiceParameterDefinition(repo, versies as String[], '')
}

return this;
