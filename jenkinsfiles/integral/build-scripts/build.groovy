def exec() {
  wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'XTerm']) {
    sh("mvn versions:set -DnewVersion=${env.VERSION} -DgenerateBackupPoms=false")
    sh("mvn resources:resources -Dapp.image.version=${env.APP_VERSION} -Dnginx.image.version=${env.NGINX_VERSION} -Dfrontend.image.version=${env.FRONTEND_VERSION} -Dsoapuimock.image.version=${env.SOAPUI_MOCKSERVICE_VERSION}")
  }
}

return this;
