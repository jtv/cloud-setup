def exec() {
  sh "rm -rf zap && " +
    "mkdir zap && " +
    "docker run -v `pwd`/zap:/zap/wrk/:rw -t owasp/zap2docker-weekly zap-baseline.py -t http://localhost:${env.WWW_PORT} -g gen.conf -r index.html || true"
  publishHTML([
          allowMissing: false,
          alwaysLinkToLastBuild: false,
          keepAll: false,
          reportDir: 'zap',
          reportFiles: 'index.html',
          reportName: 'ZAP Scan Report'])
}

return this;
