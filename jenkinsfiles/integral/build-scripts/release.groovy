def exec() {
  sshagent(credentials: ['git-credentials']) {
    sh """
        git config user.email "noreply@jenkins.ictu"
        git config user.name "jenkins"
        git add "./*pom.xml"
        git commit -m "[Jenkins Pipeline] Change POM versions to ${env.VERSION} "
        git tag -a ${env.VERSION} -m "[Jenkins Pipeline] Create tag ${env.VERSION}"
        git push origin ${env.VERSION}
        git push --tags
    """
  }

  sh """
        mvn deploy:deploy-file -DgroupId=nl.ictu.vib -DartifactId=vib-composition -Dversion=${env.VERSION} -Dpackaging=yml -Dfile=target/classes/docker-compose.yml -DrepositoryId=maven-releases -Durl=http://admin:admin123@www.nexus.vib.ictu:8081/repository/maven-releases/
        mvn deploy:deploy-file -DgroupId=nl.ictu.vib -DartifactId=vib-composition-art -Dversion=${env.VERSION} -Dpackaging=yml -Dfile=target/classes/docker-compose.art.yml -DrepositoryId=maven-releases -Durl=http://admin:admin123@www.nexus.vib.ictu:8081/repository/maven-releases/
    """
}

return this;
